﻿select
    item.item_id,
    item.item_name,
    item.item_price,
    item.category_id,
    item_category.category_name
from
    item_category
inner join
    item
on
    item.category_id = item_category.category_id

